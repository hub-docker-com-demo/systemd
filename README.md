# systemd

System and Service Manager

# Official documentation
## systemd
* [*systemd, init — systemd system and service manager*
  ](https://www.freedesktop.org/software/systemd/man/systemd.html)
* [*systemd.directives — Index of configuration directives*
  ](https://www.freedesktop.org/software/systemd/man/systemd.directives.html)
* [*systemd.exec — Execution environment configuration*
  ](https://www.freedesktop.org/software/systemd/man/systemd.exec.html)

## systemctl
* [*systemctl — Control the systemd system and service manager*
  ](https://www.freedesktop.org/software/systemd/man/systemctl.html)

# Unofficial documentation
* [*How to Auto Execute Commands/Scripts During Reboot or Startup*
  ](https://www.tecmint.com/auto-execute-linux-scripts-during-reboot-or-startup/)
  2017-02-13 Gabriel Cánepa
* [*What is an init system?*
  ](https://fedoramagazine.org/what-is-an-init-system/)
  2015-10-21 Ryan Lerch
* [*Understanding and administering systemd*
  ](https://docs.fedoraproject.org/en-US/quick-docs/understanding-and-administering-systemd/index.html)
* [*SysVinit to Systemd Cheatsheet*
  ](https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet)
* [*systemd: Converting sysvinit scripts*
  ](https://fedoramagazine.org/systemd-converting-sysvinit-scripts/)
  2015-11-04 Jon Stanley
* [*Getting started with systemd*](https://coreos.com/os/docs/latest/getting-started-with-systemd.html)

# StandardInput
* [systemd [Service] StandardInput](https://google.com/search?q=systemd+%5BService%5D+StandardInput)
* [*Why “StandardInput=tty” in oneshot systemd unit files?*
  ](https://stackoverflow.com/questions/44136655/why-standardinput-tty-in-oneshot-systemd-unit-files)
  (2018)

# Autologin
* [*Auto login and `startx` without a display manager (systemd)*
  ](http://forums.debian.net/viewtopic.php?f=16&t=123694)
  2015-07 Head_on_a_Stick
* [*getty*](https://wiki.archlinux.org/index.php/Getty)
  ArchWiki

# Docker and systemd
* Daniel Walsh serie:
  * [*How to run systemd in a container*
    ](https://developers.redhat.com/blog/2019/04/24/how-to-run-systemd-in-a-container/):
    podman
    2019-04-24 Daniel Walsh
  * [*Running systemd in a non-privileged container*
    ](https://developers.redhat.com/blog/2016/09/13/running-systemd-in-a-non-privileged-container/)
    2016-09-13 Daniel Walsh
  * [*Running systemd within a Docker Container*
    ](https://developers.redhat.com/blog/2014/05/05/running-systemd-within-docker-container/)
    2014-05-05 Daniel Walsh
* systemd docker unpriviledged
* [*Docker Tips: Running a Container With a Non Root User*
  ](https://medium.com/better-programming/running-a-container-with-a-non-root-user-e35830d1f42a):
  Methods and examples
  2018-09-20 Luc Juggery
* [*Towards unprivileged container builds*
  ](https://kinvolk.io/blog/2018/04/towards-unprivileged-container-builds/)
  2018-04-25 Alban Crequy

* Explantation on why it does not really work for CI:
  * https://hub.docker.com/_/centos/
